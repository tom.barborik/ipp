class InterpretException(Exception):
    def __init__(self, id, name="", text=""):
        self.__id = id
        self.name = name
        self.text = text


class Variable:
    def __init__(self, name, type=None, value=None):
        self.__name = name
        self.type = type
        self.value = value

    def getName(self):
        return self.__name

    def __str__(self):
        return self.__name


class Frame:
    def __init__(self):
        self.__variables = []

    def exists(self, var):
        for v in self.__variables:
            if v.getName() == var.getName():
                return True
        return False

    def add(self, var):
        if self.exists(var):
            raise InterpretException(59, "Frame error", "Variable already defined.")

        self.__variables.append(var)

    def get(self, varName):
        for v in self.__variables:
            if v.getName() == varName:
                return v

        raise InterpretException(54, "Frame error", "Variable not defined.")

    def __str__(self):
        text = str(len(self.__variables)) + "("
        f = True
        for var in self.__variables:
            if not f:
                text += ", "
            text += str(var)
            f = False
        text += ")"
        return text

    def __repr__(self):
        return self.__str__()


class Instructions:
    def createframe(self, interpret):
        interpret.getframes()["temporary"] = Frame()

    def pushframe(self, interpret):
        frames = interpret.getframes()
        if frames["temporary"] is None:
            raise InterpretException(55, "Frame error", "Temporary frame not created.")

        frames["local"].append(frames["temporary"])
        frames["temporary"] = None

    def popframe(self, interpret):
        frames = interpret.getframes()
        if len(frames["local"]) == 0:
            raise InterpretException(55, "Frame error", "Local frame does not exist.")

        frames["temporary"] = frames["local"].pop()

    def defvar(self, interpret, name):
        frames = interpret.getframes()
        frame = None

        if name[:2] == "GF":
            frame = frames["global"]
        elif name[:2] == "LF":
            if len(frames["local"]) == 0:
                raise InterpretException(55, "Frame error", "Local frame does not exist.")

            frame = frames["local"][-1]
        elif name[:2] == "TF":
            if frames["temporary"] is None:
                raise InterpretException(55, "Frame error", "Temporary frame not created.")

            frame = frames["temporary"]
        else:
            raise InterpretException(32, "Variable location error", "Variable location is not known.")

        if name[2] != '@':
            raise InterpretException(32, "Variable name error", "Variable name is wrong formatted.")

        frame.add(Variable(name[3:]))


class Interpret:
    currentOrder = 0
    __frames = {'global': Frame(), 'temporary': None, 'local': []}
    labels = []

    def __init__(self, file):
        self.inputFile = file

    def getframes(self):
        return self.__frames


interpret = Interpret(None)
instructions = Instructions()
instructions.createframe(interpret)
instructions.pushframe(interpret)
instructions.createframe(interpret)
instructions.pushframe(interpret)
instructions.defvar(interpret, "LF@test")
instructions.defvar(interpret, "LF@tet")
instructions.popframe(interpret)
print(interpret.getframes())
