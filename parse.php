<?php
/**
 * První část projektu pro předmět IPP
 * Parser
 *
 * @author Tom Barbořík <xbarbo06@stud.fit.vutbr.cz>
 */

class Scanner
{
    const T_INSTRUCTION = -1;

    const T_CONTOLOPER = 0;
    const T_HEADER = 1;
    const T_MOVE = 2;
    const T_CREATEFRAME = 3;
    const T_PUSHFRAME = 4;
    const T_POPFRAME = 5;
    const T_DEFVAR = 6;
    const T_CALL = 7;
    const T_RETURN = 8;

    const T_STACKOPER = 10;
    const T_PUSHS = 11;
    const T_POPS = 12;

    const T_MATHOPER = 20;
    const T_ADD = 21;
    const T_SUB = 22;
    const T_MUL = 23;
    const T_IDIV = 24;
    const T_LT = 25;
    const T_GT = 26;
    const T_EQ = 27;
    const T_AND = 28;
    const T_OR = 29;
    const T_NOT = 30;
    const T_INT2CHAR = 31;
    const T_STRI2INT = 32;

    const T_IOOPER = 40;
    const T_READ = 41;
    const T_WRITE = 42;

    const T_STROPER = 50;
    const T_CONCAT = 51;
    const T_STRLEN = 52;
    const T_GETCHAR = 53;
    const T_SETCHAR = 54;

    const T_TYPEOPER = 60;
    const T_TYPE = 61;

    const T_FLOWOPER = 70;
    const T_LABEL = 71;
    const T_JUMP = 72;
    const T_JUMPIFEQ = 73;
    const T_JUMPIFNEQ = 74;

    const T_DEBUGOPER = 80;
    const T_DPRINT = 81;
    const T_BREAK = 82;
    const T_LASTINSTRUCTION = 89;
    const T_FIRSTINSTRUCTION = 1;

    const T_VALUEOPER = 90;
    const T_INT = 91;
    const T_STRING = 92;
    const T_FLOAT = 93;
    const T_BOOL = 94;
    const T_VARTF = 95;
    const T_VARLF = 96;
    const T_VARGF = 97;
    const T_VALUE = 98;
    const T_PARLABEL = 99;
    const T_TYPEINT = 100;
    const T_TYPEFLOAT = 101;
    const T_TYPEBOOL = 102;
    const T_TYPESTRING = 103;
    const T_PARAM = 104;

    const T_ENDF = INF;

    const S_START = 1;
    const S_COMMENT = 2;
    const S_HEADER = 3;
    const S_INSTRUCTION = 4;
    const S_POSTINSTRUCTION = 7;
    const S_PREPARAM = 6;
    const S_PARAM = 5;
    const S_END = INF;

    private $instructionList = [
        Scanner::T_MOVE => "MOVE", Scanner::T_CREATEFRAME => "CREATEFRAME", Scanner::T_PUSHFRAME => "PUSHFRAME",
        Scanner::T_POPFRAME => "POPFRAME", Scanner::T_DEFVAR => "DEFVAR", Scanner::T_CALL => "CALL", Scanner::T_RETURN => "RETURN",
        Scanner::T_PUSHS => "PUSHS", Scanner::T_POPS => "POPS", Scanner::T_ADD => "ADD", Scanner::T_SUB => "SUB",
        Scanner::T_MUL => "MUL", Scanner::T_IDIV => "IDIV", Scanner::T_LT => "LT", Scanner::T_GT => "GT", Scanner::T_EQ => "EQ",
        Scanner::T_AND => "AND", Scanner::T_OR => "OR", Scanner::T_NOT => "NOT", Scanner::T_INT2CHAR => "INT2CHAR", Scanner::T_STRI2INT => "STR2INT",
        Scanner::T_READ => "READ", Scanner::T_WRITE => "WRITE", Scanner::T_CONCAT => "CONCAT", Scanner::T_STRLEN => "STRLEN",
        Scanner::T_GETCHAR => "GETCHAR", Scanner::T_SETCHAR => "SETCHAR", Scanner::T_TYPE => "TYPE", Scanner::T_LABEL => "LABEL", Scanner::T_JUMP => "JUMP",
        Scanner::T_JUMPIFEQ => "JUMPIFEQ", Scanner::T_JUMPIFNEQ => "JUMPIFNEQ", Scanner::T_DPRINT => "DPRINT", Scanner::T_BREAK => "BREAK"
    ];

    private $inputFile;
    private $prevToken = ["id" => 0, "value" => null];
    private $token = ["id" => 0, "value" => null];

    private $nextState = self::S_START;
    private $state;
    private $error;

    public function __construct($inputFile)
    {
        $this->inputFile = $inputFile;
        $this->error = 0;
    }

    public function getError()
    {
        return $this->error;
    }

    public function getCurrentToken()
    {
        return $this->token;
    }

    public function getPreviousToken()
    {
        return $this->prevToken;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getNextToken()
    {
        $this->prevToken = $this->token;
        $this->scanToken();

        return $this->token;
    }

    public function isVar($token)
    {
        return $token["id"] == self::T_VARGF || $token["id"] == self::T_VARLF || $token["id"] == self::T_VARTF;
    }

    public function isType($token)
    {
        return $token["id"] == self::T_TYPEINT || $token["id"] == self::T_TYPEBOOL || $token["id"] == self::T_TYPESTRING;
    }

    private function isWhitespace($text)
    {
        return ctype_space($text) && ord($text) != 10;
    }

    private function getInstructionTokenId(&$instruction)
    {
        foreach ($this->instructionList as $id => $name) {
            if (strtoupper($instruction) == $name) {
                $instruction = strtoupper($instruction);
                return $id;
            }
        }

        $this->error = 1;
        return null;
    }

    /**
     * @param $param
     * @return int
     * @throws Exception
     */
    private function analyzeParam(&$param)
    {
        $id = self::T_PARLABEL;
        if ($this->getPreviousToken()["id"] == self::T_LABEL)
            return $id;

        if (($i = strpos($param, "@")) !== false) {
            if (false && substr_count($param, "@") > 1) {
                $this->error = true;
            } else {
                switch (substr($param, 0, $i)) {
                    case "int" :
                        $id = self::T_INT;
                        $param = substr($param, 4);
                        if (!is_numeric($param))
                            throw new Exception("Int není zapsán správně.", 21);

                        break;
                    case "float":
                        $id = self::T_FLOAT;
                        $param = substr($param, 6);
                        if (!is_float($param))
                            throw new Exception("Float není zapsán správně.", 21);

                        break;
                    case "bool":
                        $id = self::T_BOOL;
                        $param = substr($param, 5);
                        if ($param != "true" && $param != "false")
                            throw new Exception("Bool není zapsán správně.", 21);

                        break;
                    case "string":
                        $id = self::T_STRING;
                        $param = substr($param, 7);
                        break;
                    case "TF":
                        $id = self::T_VARTF;
                        break;
                    case "LF":
                        $id = self::T_VARLF;
                        break;
                    case "GF":
                        $id = self::T_VARGF;
                        break;
                    default:
                        $this->error = true;
                }
            }
        } elseif ($this->prevToken["id"] != self::T_LABEL) {
            if ($param == "float")
                $id = self::T_TYPEFLOAT;
            elseif ($param == "int")
                $id = self::T_TYPEINT;
            elseif ($param == "string") {
                $id = self::T_TYPESTRING;
            } elseif ($param == "bool") {
                $id = self::T_TYPEBOOL;
            }
        }

        if ($id == self::T_VARTF || $id == self::T_VARLF || $id == self::T_VARGF) {
            if (preg_match("/^[a-zA-Z0-9\_\-\$\&\%\*]+$/", substr($param, 3)) != 1)
                throw new Exception("Zápis proměnné není validní.", 21);
        }

        return $id;
    }

    /**
     * @throws Exception
     */
    private function scanToken()
    {
        $this->state = $this->nextState;
        $this->nextState = self::S_START;

        while ($this->state != self::S_END) {
            $char = fgetc($this->inputFile);

            if ($this->state == self::S_START && feof($this->inputFile)) {
                $this->token["id"] = self::T_ENDF;
                $this->token["value"] = null;
                $this->state = self::S_END;
            } elseif ($char === FALSE) {
                if ($this->state == self::S_PARAM) {
                    $this->token["id"] = $this->analyzeParam($this->token["value"]);
                }
                $this->state = self::S_END;
            }

            switch ($this->state) {
                case self::S_START:
                    if ($char == "#") {
                        $this->state = self::S_COMMENT;
                    } elseif ($char == ".") {
                        $this->state = self::S_HEADER;
                        $this->token["value"] = $char;
                        $this->token["id"] = self::T_HEADER;
                    } elseif (!$this->isWhitespace($char) && ord($char) != 10) {
                        $this->state = self::S_INSTRUCTION;
                        $this->token["value"] = $char;
                        $this->token["id"] = self::T_INSTRUCTION;
                    } else {
                        $this->state = self::S_START;
                    }

                    break;

                case self::S_HEADER:
                    if (ord($char) == 10 || $char == "#") {
                        $this->state = self::S_END;

                        if ($char == "#")
                            fseek($this->inputFile, -1, SEEK_CUR);
                    } elseif ($this->isWhitespace($char)) {
                        $this->state = self::S_END;
                        $this->error = 1;
                    } else {
                        $this->token["value"] .= $char;
                    }

                    break;

                case self::S_COMMENT:
                    if ($char == "\n")
                        $this->state = self::S_START;

                    break;

                case self::S_INSTRUCTION:
                    if (ord($char) == 10) {
                        $this->token["id"] = $this->getInstructionTokenId($this->token["value"]);
                        $this->state = self::S_END;
                    } elseif ($char == "#") {
                        $this->token["id"] = $this->getInstructionTokenId($this->token["value"]);
                        $this->state = self::S_END;
                        fseek($this->inputFile, -1, SEEK_CUR);
                    } elseif ($this->isWhitespace($char)) {
                        $this->token["id"] = $this->getInstructionTokenId($this->token["value"]);
                        $this->state = self::S_END;
                        $this->nextState = self::S_POSTINSTRUCTION;
                    } else {
                        $this->token["value"] .= $char;
                    }

                    break;

                case self::S_POSTINSTRUCTION:
                    if (ord($char) == 10) {
                        $this->state = self::S_END;
                    } elseif ($char == "#") {
                        $this->state = self::S_COMMENT;
                        fseek($this->inputFile, -1, SEEK_CUR);
                    } elseif (!$this->isWhitespace($char)) {
                        $this->state = self::S_PARAM;
                        $this->token["id"] = self::T_PARAM;
                        $this->token["value"] = $char;
                    }

                    break;

                case self::S_PREPARAM:
                    if (ord($char) == 10) {
                        $this->state = self::S_START;
                    } elseif ($char == "#") {
                        $this->state = self::S_COMMENT;
                        fseek($this->inputFile, -1, SEEK_CUR);
                    } elseif (!$this->isWhitespace($char)) {
                        $this->state = self::S_PARAM;
                        $this->token["id"] = self::T_PARAM;
                        $this->token["value"] = $char;
                    }

                    break;

                case self::S_PARAM:
                    if (ord($char) == 10) {
                        $this->state = self::S_END;
                        $this->token["id"] = $this->analyzeParam($this->token["value"]);
                    } elseif ($char == "#") {
                        $this->state = self::S_END;
                        $this->token["id"] = $this->analyzeParam($this->token["value"]);
                        fseek($this->inputFile, -1, SEEK_CUR);
                    } elseif ($this->isWhitespace($char)) {
                        $this->state = self::S_END;
                        $this->nextState = self::S_PREPARAM;
                        $this->token["id"] = $this->analyzeParam($this->token["value"]);
                    } else {
                        $this->token["value"] .= $char;
                    }

                    break;
            }
        }
    }
}

class Parser
{
    const LANGUAGE_CODE = ".IPPcode18";
    const P_VAR = 1;
    const P_LIT_INT = 2;
    const P_LIT_BOOL = 4;
    const P_LIT_STR = 8;
    const P_LABEL = 16;
    const P_TYPE = 32;
    const P_LIT = self::P_LIT_INT | self::P_LIT_BOOL | self::P_LIT_STR;
    const P_SYM = self::P_VAR | self::P_LIT;

    private $instructionRules = [
        Scanner::T_MOVE => [self::P_VAR, self::P_SYM], Scanner::T_DEFVAR => [self::P_VAR], Scanner::T_CALL => [self::P_LABEL],
        Scanner::T_PUSHS => [self::P_SYM], Scanner::T_POPS => [self::P_VAR], Scanner::T_ADD => [self::P_VAR, self::P_VAR | self::P_LIT_INT, self::P_VAR | self::P_LIT_INT],
        Scanner::T_SUB => [self::P_VAR, self::P_VAR | self::P_LIT_INT, self::P_VAR | self::P_LIT_INT], Scanner::T_MUL => [self::P_VAR, self::P_VAR | self::P_LIT_INT, self::P_VAR | self::P_LIT_INT],
        Scanner::T_IDIV => [self::P_VAR, self::P_VAR | self::P_LIT_INT, self::P_VAR | self::P_LIT_INT], Scanner::T_LT => [self::P_VAR, self::P_SYM, self::P_SYM],
        Scanner::T_GT => [self::P_VAR, self::P_SYM, self::P_SYM], Scanner::T_EQ => [self::P_VAR, self::P_SYM, self::P_SYM],
        Scanner::T_AND => [self::P_VAR, self::P_VAR | self::P_LIT_BOOL, self::P_VAR | self::P_LIT_BOOL], Scanner::T_OR => [self::P_VAR, self::P_VAR | self::P_LIT_BOOL, self::P_VAR | self::P_LIT_BOOL],
        Scanner::T_NOT => [self::P_VAR, self::P_VAR | self::P_LIT_BOOL], Scanner::T_INT2CHAR => [self::P_VAR, self::P_VAR | self::P_LIT_INT],
        Scanner::T_STRI2INT => [self::P_VAR, self::P_VAR | self::P_LIT_STR, self::P_VAR | self::P_LIT_INT], Scanner::T_READ => [self::P_VAR, self::P_TYPE], Scanner::T_WRITE => [self::P_SYM],
        Scanner::T_CONCAT => [self::P_VAR, self::P_VAR | self::P_LIT_STR, self::P_VAR | self::P_LIT_STR], Scanner::T_STRLEN => [self::P_VAR, self::P_VAR | self::P_LIT_STR],
        Scanner::T_GETCHAR => [self::P_VAR, self::P_VAR | self::P_LIT_STR, self::P_VAR | self::P_LIT_INT], Scanner::T_SETCHAR => [self::P_VAR, self::P_VAR | self::P_LIT_INT, self::P_VAR | self::P_LIT_STR],
        Scanner::T_TYPE => [self::P_VAR, self::P_SYM], Scanner::T_LABEL => [self::P_LABEL], Scanner::T_JUMP => [self::P_LABEL],
        Scanner::T_JUMPIFEQ => [self::P_LABEL, self::P_SYM, self::P_SYM], Scanner::T_JUMPIFNEQ => [self::P_LABEL, self::P_SYM, self::P_SYM],
        Scanner::T_DPRINT => [self::P_SYM]
    ];

    private $scanner;
    private $outputFile;
    private $xw;
    private $order;
    private $argumentNumber;
    private $currentInstruction;

    /**
     * @param Scanner $scanner
     * @param $outputFile
     */
    public function __construct(\Scanner $scanner, $outputFile)
    {
        $this->scanner = $scanner;
        $this->outputFile = $outputFile;
        $this->xw = new XMLWriter();
        $this->order = 0;
        $this->argumentNumber = 0;
        $this->currentInstruction = null;
    }

    /**
     * Vrací obsah generovaného xml
     * dokumentu.
     * @return string
     */
    public function output()
    {
        return $this->xw->outputMemory();
    }

    /**
     * Inicializuje generovaný dokument.
     * beginning.
     */
    private function initXml()
    {
        $this->xw->openMemory();
        $this->xw->setIndent(true);
        $this->xw->startDocument("1.0", "UTF-8");
        $this->xw->startElement("program"); // <program>
    }

    /**
     * Ukončuje generovaný dokument.
     */
    private function endXml()
    {
        $this->xw->endElement(); // </program>
        $this->xw->endDocument();
    }

    /**
     * Začne parsování. V cyklu získává od scanneru jednotlivé tokeny a ty
     * poté posílá ke zpracování. Končí, pokud se narazilo na konec
     * souboru nebo se vyskytla chyba buď při
     * lexikální nebo syntaktické
     * analýze.
     * @throws Exception
     */
    public function start()
    {
        $this->initXml();

        do {
            $token = $this->scanner->getNextToken();
            $this->processToken($token);
        } while ($token["id"] != Scanner::T_ENDF && $this->scanner->getError() == 0);

        if ($this->scanner->getError() > 0) {
            throw new Exception("Chyba scanneru.", 21);
        }

        $this->endXml();
    }

    /**
     * Podle typu tokenu provede jeho zpracování.
     * @param $token
     * @throws Exception
     */
    private function processToken($token)
    {
        if ($token["id"] == Scanner::T_HEADER) { // hlavička
            $this->pHeader($token);
        } else if ($token["id"] >= Scanner::T_FIRSTINSTRUCTION && $token["id"] <= Scanner::T_LASTINSTRUCTION) { // instrukce
            $this->pInstruction($token);
        } else if ($token["id"] != Scanner::T_ENDF) { // parametr
            $this->pParam($token);
        } else {
            $this->endInstruction();
        }
    }

    /**
     * Zpracování hlavičky programu.
     * @param $token
     * @throws Exception
     */
    private function pHeader($token)
    {
        if ($this->order != 0) { // hlavička musí být první & jenom jedna
            throw new Exception("Hlavička není na správném místě.", 21);
        }

        if (strtolower($token["value"]) != strtolower(self::LANGUAGE_CODE)) {
            throw new Exception("Hlavička není ve správném formátů.", 21);
        }

        $this->xw->startAttribute("language");
        $this->xw->text(substr(self::LANGUAGE_CODE, 1));
        $this->xw->endAttribute();

        $this->order++;
    }

    /**
     * Zpracování instrukce a kontrola, zda nechybí hlavička
     * programu.
     * @param $token
     * @throws Exception
     */
    private function pInstruction($token)
    {
        if ($this->order == 0) {
            throw new Exception("Chybí hlavička programu.", 21);
        }

        $this->endInstruction();

        $this->currentInstruction = $token["id"];
        $this->argumentNumber = 0;
        $this->xw->startElement("instruction");

        $this->xw->startAttribute("order");
        $this->xw->text($this->order++);
        $this->xw->endAttribute();

        $this->xw->startAttribute("opcode");
        $this->xw->text($token["value"]);
        $this->xw->endAttribute();
    }

    /**
     * Kontrola počtu parametrů a uzavření instrukce.
     * @throws Exception
     */
    private function endInstruction()
    {
        if (!is_null($this->currentInstruction)) {
            if (array_key_exists($this->currentInstruction, $this->instructionRules) && $this->argumentNumber < count($this->instructionRules[$this->currentInstruction])) // nedostatečný počet parametrů
                throw new Exception("(" . ($this->order - 1) . ") " . "Nedostatečný počet parametrů instrukce.", 21);

            $this->xw->endElement();
        }
    }

    /**
     * Zpracování parametru instrukce.
     * @param $token
     * @throws Exception
     */
    private function pParam($token)
    {
        if (!array_key_exists($this->currentInstruction, $this->instructionRules) || empty($this->instructionRules[$this->currentInstruction])) // instrukce nemá žádné parametry
            throw new Exception("(" . ($this->order - 1) . " - " . $this->currentInstruction . ") " . "Instrukce nemá žádné parametry.", 21);

        if ($this->argumentNumber >= count($this->instructionRules[$this->currentInstruction])) // více argumentů než kolik jich má instruce mít
            throw new Exception("(" . ($this->order - 1) . " - " . $this->currentInstruction . ") " . "Instrukce má mít méně parametrů.", 21);

        $rules = $this->instructionRules[$this->currentInstruction][$this->argumentNumber];
        if (!($rule = $this->checkRule($token, $rules)))
            throw new Exception("(" . ($this->order - 1) . " - " . $this->currentInstruction . ") " . "Tento typ parametru není u této instrukce povolen.", 21);

        $this->xw->startElement("arg" . ++$this->argumentNumber);
        $this->xw->startAttribute("type");

        switch ($rule) {
            case self::P_VAR:
                $this->xw->text("var");
                break;
            case self::P_LIT_BOOL:
                $this->xw->text("bool");
                break;
            case self::P_LIT_INT:
                $this->xw->text("int");
                break;
            case self::P_LIT_STR:
                $this->xw->text("string");
                break;
            case self::P_LABEL:
                $this->xw->text("label");
                break;
            case self::P_TYPE:
                $this->xw->text("type");
                break;
            default:
                throw new Exception("(" . ($this->order - 1) . " - " . $this->currentInstruction . ") " . "Neexistující textová reprezentace pro pravidlo " . $rule, 99);
        }

        $this->xw->endAttribute();
        $this->xw->text($token["value"]);
        $this->xw->endElement();
    }

    /**
     * Kontroluje, zda zadaný token odpovídá nekterému z pravidel.
     * Vrací hodnotu pravidla, kterému odpovídá, jinak 0.
     * @param $token
     * @param $rules
     * @return int
     */
    private function checkRule($token, $rules)
    {
        if ($rules & self::P_VAR && $this->scanner->isVar($token)) {
            return self::P_VAR;
        } elseif ($rules & self::P_LIT_INT && $token["id"] == Scanner::T_INT) {
            return self::P_LIT_INT;
        } elseif ($rules & self::P_LIT_BOOL && $token["id"] == Scanner::T_BOOL) {
            return self::P_LIT_BOOL;
        } elseif ($rules & self::P_LIT_STR && $token["id"] == Scanner::T_STRING) {
            return self::P_LIT_STR;
        } elseif ($rules & self::P_LABEL && $token["id"] == Scanner::T_PARLABEL) {
            return self::P_LABEL;
        } elseif ($rules & self::P_TYPE && $this->scanner->isType($token)) {
            return self::P_TYPE;
        }

        return 0;
    }
}

$stdin = null;
if ($argc > 1) {
    $stdin = fopen($argv[1], 'r');
} else
    $stdin = fopen('php://stdin', 'r');

$stdout = fopen('php://stdout', 'w');
$stderr = fopen('php://stderr', 'a');

if ($stdin === false) { // chybějící vstupní soubor
    exit(11);
}

if ($stdout === false) { // chybející výstupní soubor
    exit(12);
}

$parser = new \Parser(new \Scanner($stdin), $stdout);

$return = 0;

try {
    $parser->start();
    echo $parser->output();
} catch (Exception $e) {
    if (false && $stderr && $e->getMessage() != "")
        fwrite($stderr, "(" . $e->getCode() . ") " . $e->getMessage() . "\n");
    $return = $e->getCode();
} finally {
    fclose($stdin);
    fclose($stdout);

    if($stderr)
        fclose($stderr);
}

exit($return);